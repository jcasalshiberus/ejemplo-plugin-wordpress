<?php
/*
Plugin Name: Test Plugin
Version: 1.0
*/

define( 'TEST_PLUGIN_VERSION', '1.0' );
define( 'TEST_PLUGIN__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'TEST_PLUGIN_DELETE_LIMIT', 10000 );

require_once(TEST_PLUGIN__PLUGIN_DIR . 'class.test-plugin.php');

register_activation_hook( __FILE__, array('TestPlugin', 'plugin_activation'));
register_uninstall_hook(__FILE__, array('TestPlugin', 'plugin_uninstall'));
add_action('wp_login', array('TestPlugin', 'userLogin'), 10, 2);
add_action('wp_logout', array('TestPlugin', 'userLogout'), 10, 1);

if ( is_admin() || ( defined( 'WP_CLI' ) && WP_CLI ) ) {
    require_once(TEST_PLUGIN__PLUGIN_DIR . 'class.test-plugin-admin.php');
    add_action( 'init', array( 'TestPluginAdmin', 'init' ) );
}



<?php

class TestPluginAdmin {

    public function init() {
        add_action('admin_menu', array(self::class, 'addAdminMenuItems'), 9);
        add_action('admin_init', array(self::class, 'registerAndBuildFields' ));
    }

    public function addAdminMenuItems() {
        add_menu_page(
            'Test Plugin',
            'Test Plugin',
            'administrator',
            'Test Plugin',
            array(self::class, 'displayAdminDashboard'),
            'dashicons-email',
            20
        );
    }

    public function displayAdminDashboard() {
        require_once TEST_PLUGIN__PLUGIN_DIR . 'views/admin-settings-display.php';
    }

    public function registerAndBuildFields() {
        add_settings_section(
        // ID used to identify this section and with which to register options
            'test-plugin_general_section_1',
            'Sección 1 de prueba',
            // Callback used to render the description of the section
            array(self::class, 'test_plugin_display_general_account' ),
            // Page on which to add this section of options
            'test-plugin_general_settings'
        );
        add_settings_section(
        // ID used to identify this section and with which to register options
            'test-plugin_general_section_2',
            'Sección 2 de prueba',
            // Callback used to render the description of the section
            array(self::class, 'test_plugin_display_general_account' ),
            // Page on which to add this section of options
            'test-plugin_general_settings'
        );
        unset($args);
        $args = array (
            'type'      => 'input',
            'subtype'   => 'text',
            'id'    => 'test-plugin_example_setting',
            'name'      => 'test-plugin_example_setting',
            'required' => 'false',
            'get_options_list' => '',
            'value_type'=>'normal',
            'wp_data' => 'option'
        );
        add_settings_field(
            'test-plugin_example_setting',
            'Opción de ejemplo',
            array(self::class, 'test_plugin_render_settings_field' ),
            'test-plugin_general_settings',
            'test-plugin_general_section_1',
            $args
        );


        register_setting(
            'test-plugin_general_settings',
            'test-plugin_example_setting'
        );

    }

    public function test_plugin_display_general_account() {
        echo '<p>These settings apply to all Test Plugin functionality.</p>';
    }

    public function test_plugin_render_settings_field($args) {

        if($args['wp_data'] == 'option'){
            $wp_data_value = get_option($args['name']);
        } elseif($args['wp_data'] == 'post_meta'){
            $wp_data_value = get_post_meta($args['post_id'], $args['name'], true );
        }

        switch ($args['type']) {

            case 'input':
                $value = ($args['value_type'] == 'serialized') ? serialize($wp_data_value) : $wp_data_value;
                if($args['subtype'] != 'checkbox'){
                    $prependStart = (isset($args['prepend_value'])) ? '<div class="input-prepend"> <span class="add-on">'.$args['prepend_value'].'</span>' : '';
                    $prependEnd = (isset($args['prepend_value'])) ? '</div>' : '';
                    $step = (isset($args['step'])) ? 'step="'.$args['step'].'"' : '';
                    $min = (isset($args['min'])) ? 'min="'.$args['min'].'"' : '';
                    $max = (isset($args['max'])) ? 'max="'.$args['max'].'"' : '';
                    if(isset($args['disabled'])){
                        // hide the actual input bc if it was just a disabled input the informaiton saved in the database would be wrong - bc it would pass empty values and wipe the actual information
                        echo $prependStart.'<input type="'.$args['subtype'].'" id="'.$args['id'].'_disabled" '.$step.' '.$max.' '.$min.' name="'.$args['name'].'_disabled" size="40" disabled value="' . esc_attr($value) . '" /><input type="hidden" id="'.$args['id'].'" '.$step.' '.$max.' '.$min.' name="'.$args['name'].'" size="40" value="' . esc_attr($value) . '" />'.$prependEnd;
                    } else {
                        echo $prependStart.'<input type="'.$args['subtype'].'" id="'.$args['id'].'" "'.$args['required'].'" '.$step.' '.$max.' '.$min.' name="'.$args['name'].'" size="40" value="' . esc_attr($value) . '" />'.$prependEnd;
                    }

                } else {
                    $checked = ($value) ? 'checked' : '';
                    echo '<input type="'.$args['subtype'].'" id="'.$args['id'].'" "'.$args['required'].'" name="'.$args['name'].'" size="40" value="1" '.$checked.' />';
                }
                break;
            default:
                break;
        }
    }

    public function displayAdminSettings() {
        // set this var to be used in the settings-display view
        $active_tab = isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : 'general';
        if(isset($_GET['error_message'])){
            add_action('admin_notices', array(self::class,'pluginNameSettingsMessages'));
            do_action( 'admin_notices', $_GET['error_message'] );
        }
        require_once TEST_PLUGIN__PLUGIN_DIR . 'views/admin-settings-display.php';
    }

    public function pluginNameSettingsMessages($error_message){
        switch ($error_message) {
            case '1':
                $message = __( 'There was an error adding this setting. Please try again.  If this persists, shoot us an email.', 'my-text-domain' );
                $err_code = esc_attr( 'plugin_name_example_setting' );
                $setting_field = 'plugin_name_example_setting';
                break;
        }
        $type = 'error';
        add_settings_error(
            $setting_field,
            $err_code,
            $message,
            $type
        );
    }
}
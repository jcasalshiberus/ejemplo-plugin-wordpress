<?php

class TestPlugin {
    
    const TABLE_NAME = "test_plugin_counters";
    public function plugin_activation() {
        global $wpdb;
        $tableName = $wpdb->prefix . self::TABLE_NAME;
        
        $sql = 'CREATE TABLE ' . $tableName . ' (username varchar(20), login_counter int, logout_counter int);';
        self::maybe_create_table($tableName, $sql);
    }

    public function plugin_uninstall() {
        global $wpdb;
        $tableName = $wpdb->prefix . self::TABLE_NAME;
        $sql = 'DROP TABLE ' . $tableName;
        $wpdb->query($sql);
    }

    public function userLogin($userLogin, $user) {
        global $wpdb;
        $tableName = $wpdb->prefix . self::TABLE_NAME;

        $sql = 'SELECT * FROM ' . $tableName . ' where username = \'' . $user->display_name . '\'';
        $row = $wpdb->get_results($sql, ARRAY_A);

        if (!$row || count($row) == 0) {
            $wpdb->insert($tableName, array('username' => $user->display_name, 'login_counter' => 1, 'logout_counter' => 0));

            return;
        }

        $loginCounter = $row[0]['login_counter'] + 1;

        $wpdb->update($tableName, array('login_counter' => $loginCounter), array('username' => $user->display_name));

    }

    public function userLogout($userId) {
        global $wpdb;
        $tableName = $wpdb->prefix . self::TABLE_NAME;

        $user = get_user_by('id', $userId);

        $sql = 'SELECT * FROM ' . $tableName . ' where username = \'' . $user->display_name . '\'';
        $row = $wpdb->get_results($sql, ARRAY_A);

        if (!$row || count($row) == 0) {
            $wpdb->insert($tableName, array('username' => $user->display_name, 'login_counter' => 0, 'logout_counter' => 1));

            return;
        }

        $logoutCounter = $row[0]['logout_counter'] + 1;

        $wpdb->update($tableName, array('logout_counter' => $logoutCounter), array('username' => $user->display_name));
    }

    public function maybe_create_table( $table_name, $create_ddl ) {
        global $wpdb;

        $query = $wpdb->prepare( 'SHOW TABLES LIKE %s', $wpdb->esc_like( $table_name ) );

        if ( $wpdb->get_var( $query ) === $table_name ) {
            return true;
        }

        $wpdb->query( $create_ddl );

        if ( $wpdb->get_var( $query ) === $table_name ) {
            return true;
        }

        return false;
    }
}
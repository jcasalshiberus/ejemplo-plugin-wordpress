<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'demo_wordpress' );

/** Database username */
define( 'DB_USER', 'demo_wordpress' );

/** Database password */
define( 'DB_PASSWORD', 'demo_wordpress' );

/** Database hostname */
define( 'DB_HOST', 'mariadb' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'r3W@Wj.:St9<)m5I_v[yZEkI?P;4129Mp[H%_}-I[tqwTJw11OE}fdSqHiId6+pH' );
define( 'SECURE_AUTH_KEY',  'hO6#Ru^wG^u#&.?|qnVp`+M%Rmf8=&1id9 ohA5w9p+!I.>PjENH-y}lVP~E92$M' );
define( 'LOGGED_IN_KEY',    'W0-%J^Mk/??pk`b5`XMS=C_38{A3]m@35Y4!Q.TLh*i!v1?D/to/,->vbeP,u_Jl' );
define( 'NONCE_KEY',        '%)d%PSHzgE*=]q$eyMyi|naOL=s&{(E!b!^An`uV+:Rr,(4(8#oFKJWHKVQw(aDg' );
define( 'AUTH_SALT',        '$4<5Z]On*a$u/[ n=Ic@(jdP?L1Fv 85dgdo/7#1bow].,zosx=CF{O4F=To~p3d' );
define( 'SECURE_AUTH_SALT', '=)~:UNDVtli>H(;uN!RP_luWfv`Ne4Jw-NtCb^Ky5DXhaWQo+y*I8y@CRq|BkpW}' );
define( 'LOGGED_IN_SALT',   'U eKahgXa>:L3L*a?M$9[>_ VJHTV+Et9_VRb/%2Nga_C,ah0X=T;9DDInG?Y*rQ' );
define( 'NONCE_SALT',       '>n*QzSW6ADs0*KJcHvS9St]5qjnq!Q}EkdnzO4]FyAyis c!J=LQqu%UdbS5<*al' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
    define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
